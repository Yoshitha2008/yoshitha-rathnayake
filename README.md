# <div align="center"><a href="https://yoshitharathnayake.w3spaces.com/YOSHITHA_RATHNAYAKE.html"><b><i>Yoshitha Rathnayake</i></b></a></div>

# <div align="center"><img src="Yoshitha2.png" height="250px" alt="Yoshitha Rathnayake"></div>

<a href="https://gitlab.com/YoshithaRathnayake">@YoshithaRathnayake<a>

> Hi, I am Yoshitha Rathnayake. This is my personal portfolio.


# About Me

> I am <a href="https://yoshitharathnayake.w3spaces.com/YOSHITHA_RATHNAYAKE.html"><b><i>Yoshitha Rathnayake.</i></b></a>

> I am a Student, Software Developer, Programmer, Web Developer, Game Developer, Video Editor, Photo Editor and a badminton player.

> I am 13 years old. 

> I am studing in <a href="https://www.sack.edu.lk/#gsc.tab=0"><b><i>St. Anthony's College Kandy.</i></b></a>

> My favourite subject is ICT.

> I am observing Buddhism as my religion.

> I live in Kandy District, Central Province, Sri Lanka 🇱🇰. 

> I am playing Badminton 🏸 in my school as my favourite sport.

> I am a member of <a href="https://github.com/acf-sack"><b><i>ACF Committee</i></b></a> in our school and also a memeber of <a href="https://sefglobal.org/"><b><i>Sustainable Education Foundation (SEF).</i></b></a>

> I am a Volkswagen car lover.

> I am proud to say that I am a Sri Lankan 🇱🇰, Buddhist and Antonian.


# What can I do ?

> Working with (Windows, Linux, Macintosh Operating Systems)
  
   
<a href="https://www.microsoft.com/en-in/windows"><img src="https://img.icons8.com/color/50/000000/windows-11.png"/><a>    <a href="https://www.linux.org/pages/download/"><img src="https://img.icons8.com/color/48/000000/linux--v1.png" height="55px"><a>     <a href="https://www.apple.com/macos/monterey/"><img src="https://img.icons8.com/ios-filled/50/ffffff/mac-os.png" height="50px"><a>
   
  
> Working with the CMD/PowerShell/Terminal in Windows, Linux, Macintosh (Operating Systems) 
  
  
<a href="https://www.microsoft.com/en-us/p/powershell/9mz1snwt0n5d?activetab=pivot:overviewtab#"><img src="https://deow9bq0xqvbj.cloudfront.net/image-logo/1769310/powershell.png" height="50px"><a><a href="https://linux.softpedia.com/get/Terminals/Terminal-3667.shtml"><img src="https://cdn1.macworld.co.uk/cmsdata/features/3608274/terminalicon2_thumb800.png" height="50px"><a><a href="https://sourceforge.net/directory/os:windows/?q=terminal+macos"><img src="https://th.bing.com/th/id/R.89609d9d431352f2e33b1f6e3541cd6a?rik=SAxIDO2wV5uaZg&riu=http%3a%2f%2fwww.journaldulapin.com%2fwp-content%2fuploads%2f2014%2f06%2fTerminal.png&ehk=Imywc0M3kWwm7BBNym409lajOPUIAn5tqXrF5Pb40TM%3d&risl=&pid=ImgRaw&r=0" height="50px"><a>    
    

> Working with Microsoft Office Pro Plus, Office 365, Microsoft 365, Libre Office, Google Docs (Softwares)
    
<a href="https://www.microsoft.com/en-US/microsoft-365/p/office-professional-2021/CFQ7TTC0HHJ9?activetab=pivot%3aoverviewtab"><img src="https://img.icons8.com/color/50/000000/microsoft-office-2019.png"/></a>    <a href="https://www.office.com/"><img src="https://img.icons8.com/color/50/000000/office-365.png"/></a>     <a href="https://www.libreoffice.org/"><img src="https://img.icons8.com/windows/50/ffffff/libre-office-suite.png"/></a>       <a href="https://docs.google.com/document/u/0/"><img src="https://img.icons8.com/color/50/000000/google-docs--v2.png"/></a>
    

> Working with Microsoft Edge, Chrome, Mozilla Firefox, Opera, Safari (Web Browsers)
    
<a href="https://www.microsoft.com/en-us/edge?r=1"><img src="https://img.icons8.com/color/48/4a90e2/ms-edge-new.png"/></a>      <a href="https://www.google.com/chrome/"><img src="https://img.icons8.com/fluency/48/4a90e2/chrome.png"/></a>       <a href="https://www.mozilla.org/en-US/exp/firefox/new/"><img src="https://img.icons8.com/external-tal-revivo-color-tal-revivo/43/4a90e2/external-firefox-a-free-and-open-source-web-browser-developed-by-the-mozilla-foundation-logo-color-tal-revivo.png"/></a>       <a href="https://www.opera.com/download"><img src="https://img.icons8.com/color/48/4a90e2/opera--v1.png"/></a>      <a href="https://support.apple.com/downloads/safari"><img src="https://img.icons8.com/color/48/4a90e2/safari--v1.png"/></a>
  
> Web Designing with Notepad, Notepad++, Visual Studio Code, IntelliJ, Pycharm (Softwares)


<a href="https://www.microsoft.com/en-us/p/windows-notepad/9msmlrh6lzf3?activetab=pivot:overviewtab"><img src="https://www.file-extensions.org/imgs/app-icon/128/759/microsoft-windows-notepad-icon.png" height="45px"><a>    <a href="https://notepad-plus-plus.org/downloads/"><img src="https://img.icons8.com/fluency/48/000000/notepad-plus-plus.png"/><a>  <a href="https://code.visualstudio.com/Download"><img src="https://img.icons8.com/fluency/48/000000/visual-studio-code-2019.png"/><a>    <a href="https://www.jetbrains.com/idea/download/?fromIDE=#section=windows"><img src="https://img.icons8.com/color/48/000000/intellij-idea.png"/><a>   <a href="https://www.jetbrains.com/pycharm/download/#section=windows"><img src="https://img.icons8.com/color/48/000000/pycharm.png"/><a>

> Web Designing with HTML, CSS, JavaScript, Bootstrap, Python (Programming Languages) 


<a href="https://www.w3schools.com/html/"><img src="https://img.icons8.com/color/48/000000/html-5--v1.png"/><a>    <a href="https://www.w3schools.com/css/"><img src="https://img.icons8.com/color/48/000000/css3.png"/><a>    <a href="https://www.w3schools.com/js/"><img src="https://img.icons8.com/color/48/000000/javascript--v1.png"/><a>    <a href="https://www.w3schools.com/bootstrap/"><img src="https://img.icons8.com/color/48/000000/bootstrap.png"/><a>   <a href="https://www.w3schools.com/python/"><img src="https://img.icons8.com/fluency/48/000000/python.png"/><a>
        

> Video Editing with Adobe Premiere Pro, Microsoft Video Editor, Movie Maker (Softwares)
    
<a href="https://www.adobe.com/products/premiere.html"><img src="https://img.icons8.com/material/52/4a90e2/adobe-premiere-pro.png"/></a> 
<img src="https://img.icons8.com/fluency/50/4a90e2/photos.png"/>     <img src="https://img.icons8.com/fluency/50/4a90e2/windows-movie-maker.png"/>
    
> Photo Editing with Adobe Photoshop, Microsoft Photos (Softwares)
    
<a href="https://www.adobe.com/products/photoshop.html"><img src="https://img.icons8.com/color-glass/50/4a90e2/adobe-photoshop.png"/></a> 
<img src="https://img.icons8.com/fluency/50/4a90e2/photos.png"/>
    
    
> Game Developing with RPG Maker MV (Software)


<a href="https://www.rpgmakerweb.com/products/rpg-maker-mv"><img src="https://www.autotechint.com/wp-content/uploads/rpg-maker-mv-icon-32.png" height="42px"><a>


> Programming with Scratch (Software)


<a href="https://scratch.mit.edu/download/"><img src="https://www.pngkey.com/png/full/786-7861727_scratch-desktop-4-scratch-desktop-logo.png" height="40px"><a>

  
# Contact Me
  
> Full Name :- <b><i>R.M.Y.S.B.Rathnayake</i></b>
  
> Short Name :- <b><i>Yoshitha Rathnayake</i></b>
  
> Phone Number :- <b><i>+94767073488</i></b>
  
> Email :- <a href="https://mail.google.com/mail/u/0/?fs=1&to=yoshithabandara2008@gmail.com&tf=cm" ><b><i>yoshithabandara2008@gmail.com</i></b></a>
  
  
# Follow Me
  
  
<a href=" https://chat.whatsapp.com/KRqihLP7J7HEpM2dL6Zwt9" ><img src="https://img.icons8.com/color/48/000000/whatsapp--v1.png" alt="WhatsApp Icon" /></a>    <a href="https://t.me/joinchat/hP-WbTXCf185YTg1" ><img src="https://img.icons8.com/color/48/000000/telegram-app--v1.png" alt="Telegram Icon" /></a>   <a href="https://github.com/YoshithaRathnayake2008" ><img src="https://img.icons8.com/fluency/48/000000/github.png" alt="Git Hub Icon" /></a>    <a href="https://gitlab.com/YoshithaRathnayake" ><img src="https://img.icons8.com/color/48/000000/gitlab.png" alt="Git Lab Icon"/></a>    <a href="https://twitter.com/YoshithaSenesh" ><img src="https://img.icons8.com/color/48/000000/twitter-circled--v1.png" alt="Twitter Icon" /></a>     <a href="https://www.instagram.com/yoshitha_rathnayake/" ><img src="https://img.icons8.com/color/48/000000/instagram-new.png" alt="Instagram Icon" /></a>

**Visitors Count**
  
![VisitorCount](https://profile-counter.glitch.me/{yoshitharathnayake2008}/count.svg)
  

